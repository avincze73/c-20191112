1. Feladat
�rjon programot, amely billenty�zetr�l eg�sz �rt�keket olvas be a 
0 v�gjelig. 
A program �rja k�perny�re azokat az �rt�keket, amelyek megegyeznek 
az el�z� k�t �rt�k �sszeg�vel.


2. Feladat
�rjon programot, amely a v�letlen bolyong�st mutatja egy 10x10-es t�mb�n. 
A t�mb elemei a '.' karakterrel vannak inicializ�lva, majd a program a v�letlen
bolyong�s sor�n a l�togatott cell�k tartalm�t m�dos�tja a-z-ig terjed� karakterekkel. 
Amennyiben minden ir�ny blokkolt, a program befejezi fut�s�t.

3. Feladat
Hat�rozzuk meg egy t�mb elemeinek a maximum�t.

4. Feladat
Hat�rozzuk meg egy t�mb elemei k�z�l a p�ros elemek poz�ci�j�t,
ahol a t�mb elemei v�letlenszer�en gener�lt sz�mok.

5. Feladat
Vizsg�ljuk meg, hogy egy t�mb csak p�ros sz�mokat 
tartalmaz-e vagy sem.

6. Feladat
Hat�rozzuk meg egy adott sz�m sz�mjegyeinek az �sszeg�t

7. Feladat
�rjon f�ggv�nyt, amely megkeresi egy t�mb legnagyobb �s legkisebb elem�t.