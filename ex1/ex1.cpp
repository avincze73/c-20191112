// ex1.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
//https://gitlab.com/avincze73/c-20191112

int main(int argc, char** argv) {
	int t[3] = { 0 };
	int harmadik = 0;
	int index = -1;
	for (;;) {
		scanf("%d", &t[index = (index + 1) % 3]);
		if (!t[index]) {
			break;
		}
		if (index == 2) {
			harmadik = 1;
		}
		if (harmadik && 
			(t[index] == (t[(index + 1) % 3] + 
				t[(index + 2) % 3]))) {
			printf("%d\n", t[index]);
		}

	}
	return (EXIT_SUCCESS);
}



