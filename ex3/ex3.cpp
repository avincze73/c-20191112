// ex3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int main()
{
	int tomb[] = {1,2,3,4,5,6,7,8,9};
	int legnagyobb = tomb[0];
	for (size_t i = 1; i < sizeof(tomb)/sizeof(int); i++)
	{
		if (tomb[i] > legnagyobb)
		{
			legnagyobb = tomb[i];
		}
	}
	printf("%d\n", legnagyobb);
    return 0;
}

