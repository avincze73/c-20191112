// ex5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int main()
{
	int tomb[] = {2,2,2,2,2,6,2,8,2};
	int paros = 1;
	for (size_t i = 1; i < sizeof(tomb) / sizeof(int); i++)
	{
		if ( (tomb[i]%2) != 0 )
		{
			paros = 0;
			break;
		}
	}
	printf("%d\n", paros);
    return 0;
}

