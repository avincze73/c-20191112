// pointer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>

double average(int* t, int length)
{
	int sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		sum += t[i];
	}
	return (double)sum / length;
}

void average2(int* t, int length,
	double* avg)
{
	int sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		sum += t[i];
	}
	*avg = (double)sum / length;
}


void analyze(int* t, int length,
	int* max, int* min, int* sum, double* avg)
{
	*max = t[0];
	*min = t[0];
	for (size_t i = 0; i < length; i++)
	{
		*sum += t[i];
		*max = t[i] > *max ? t[i] : *max;
		*min = t[i] < *min ? t[i] : *min;
	}
	*avg = (double)(*sum) / length;
}

void f(int* i)
{
	printf("%d\n", *i);
	++(*i);
	printf("%d\n", *i);
}

int* getArray()
{
	int* t = (int*)malloc(10*sizeof(int));
	for (size_t i = 0; i < 10; i++)
	{
		t[i] = i + 1;
	}
	return t;
}



int** getNumbers()
{
	int** result = (int**)malloc(6*sizeof(int*));
	
	result[0] =(int*) malloc(1*sizeof(int));
	result[0][0] = 1;

	result[1] = (int*)malloc(2 * sizeof(int));
	result[1][0] = 2;
	result[1][1] = 3;

	result[2] = (int*)malloc(3 * sizeof(int));
	result[2][0] = 4;
	result[2][1] = 5;
	result[2][2] = 6;

	result[3] = (int*)malloc(4 * sizeof(int));
	result[3][0] = 7;
	result[3][1] = 8;
	result[3][2] = 9;
	result[3][3] = 10;

	result[4] = (int*)malloc(5 * sizeof(int));
	result[4][0] = 11;
	result[4][1] = 12;
	result[4][2] = 13;
	result[4][3] = 14;
	result[4][4] = 15;

	result[5] = (int*)malloc(6 * sizeof(int));
	result[5][0] = 16;
	result[5][1] = 17;
	result[5][2] = 18;
	result[5][3] = 19;
	result[5][4] = 20;
	result[5][5] = 21;
	return result;
}

void freeNumbers(int** p)
{
	for (size_t i = 0; i < 6; i++)
	{
		free(p[i]);
	}
	free(p);
}

int accum(int* t, int length, int init,
	int(*operation)(int, int),
	bool (*condition)(int))
{
	for (size_t i = 0; i < length; i++)
	{
		if (condition(t[i]))
		{
			init = operation(init, t[i]);
		}
	}
	return init;
}

int add(int a, int b)
{
	return a + b;
}

int multiply(int a, int b)
{
	return a * b;
}

bool positive(int a)
{
	return a > 0;
}

bool even(int a)
{
	return a % 2 == 0;
}

typedef int(*OperationType)(int, int);
typedef bool(*ConditionType)(int);

int main()
{
	int test = 10;
	f(&test);
	printf("%d\n", test);



	int* i=(int*)malloc(10*sizeof(int));
	for (size_t j = 0; j < 10; j++)
	{
		i[j] = j + 1;
	}

	for (size_t j = 0; j < 10; j++)
	{
		printf("%d\n", i[j]);
	}

	double myaverage = average(i, 10);


	double myaverage2 = 0;
	average2(i, 10, &myaverage2);

	int min = 0, max = 0, sum = 0;
	analyze(i, 10, &max, &min, &sum, &myaverage2);
	printf("%d, %d, %d, %f\n", 
		max, min, sum, myaverage);

	
	printf("Average: %f\n", myaverage);
	free(i);
	i = NULL;


	int* myarray = getArray();
	for (size_t i = 0; i < 10; i++)
	{
		printf("%d\n", myarray[i]);
	}
	free(myarray);


	int** mystructure = getNumbers();
	int count = 0;
	for (size_t i = 0; i < 6; i++)
	{
		for (int j = 0; j <= count; j++)
		{
			printf("%d ", mystructure[i][j]);
		}
		printf("\n");
		count++;
	}

	freeNumbers(mystructure);


	/*accum*/
	OperationType op[] = { add, multiply };
	ConditionType co[] = {positive, even};


	int aa[] = {1,2,3,4,5,6,7,8,9,10};
	printf("%d\n", accum(aa, 10, 0, op[0], co[0]));
	printf("%d\n", accum(aa, 10, 1, op[1], co[1]));


	return 0;
}

